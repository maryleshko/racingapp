import UIKit

class SettingsViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var obstacleImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var chooseFirstCarButton: UIButton!
    @IBOutlet weak var chooseSecondCarButton: UIButton!
    @IBOutlet weak var chooseThirdCarButton: UIButton!
    @IBOutlet weak var chooseFirstObstacleButton: UIButton!
    @IBOutlet weak var chooseSecondObstacleButton: UIButton!
    @IBOutlet weak var chooseThirdObstacleButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var buttonSavedSettings: UIButton!
    @IBOutlet weak var fillInLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    // MARK: - Public Properties
    
    var settings: Settings?
    override var prefersStatusBarHidden: Bool {
        return true
    }
    let key = "settings"
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameTextField.delegate = self

        self.settings = SettingsManager.shared.getSettings()
        
        self.setTitles()
        self.setSettings()
        
        self.registerForKeyboardNotifications()
        
        
        let textFieldCloseRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDetected(_:)))
        textFieldCloseRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(textFieldCloseRecognizer)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        self.settings?.username = nameTextField.text
        
        if let settings = self.settings {
        SettingsManager.shared.setSettings(settings)
        }
        
    }
    
    // MARK: - IBActions

    @IBAction func chooseFirstCarButton(_ sender: UIButton) {
        
        self.carImageView.image = UIImage(named: Constants.carsArray.first ?? "grayCarImage")
        self.settings?.car = Constants.carsArray.first
        self.carImageView.contentMode = .scaleAspectFit
        chooseFirstCarButton.isSelected = true
        chooseSecondCarButton.isSelected = false
        chooseThirdCarButton.isSelected = false
    }
    @IBAction func chooseSecondCarButton(_ sender: UIButton) {
        self.carImageView.image = UIImage(named: Constants.carsArray[1])
        self.settings?.car = Constants.carsArray[1]
        self.carImageView.contentMode = .scaleAspectFit
        chooseFirstCarButton.isSelected = false
        chooseSecondCarButton.isSelected = true
        chooseThirdCarButton.isSelected = false
    }
    @IBAction func chooseThirdCarButton(_ sender: UIButton) {
        self.carImageView.image = UIImage(named: Constants.carsArray[2])
        self.settings?.car = Constants.carsArray[2]
        self.carImageView.contentMode = .scaleAspectFit
        chooseFirstCarButton.isSelected = false
        chooseSecondCarButton.isSelected = false
        chooseThirdCarButton.isSelected = true
    }
    @IBAction func chooseFirstObstacleButton(_ sender: UIButton) {
        obstacleImageView.image = UIImage(named: Constants.obstaclesArray.first ?? "stoneCarImage")
        self.settings?.obstacle = Constants.obstaclesArray.first
        self.carImageView.contentMode = .scaleAspectFit
        chooseFirstObstacleButton.isSelected = true
        chooseSecondObstacleButton.isSelected = false
        chooseThirdObstacleButton.isSelected = false
    }
    @IBAction func chooseSecondObstacleButton(_ sender: UIButton) {
        obstacleImageView.image = UIImage(named: Constants.obstaclesArray[1])
        self.settings?.obstacle = Constants.obstaclesArray[1]
        self.carImageView.contentMode = .scaleAspectFit
        chooseFirstObstacleButton.isSelected = false
        chooseSecondObstacleButton.isSelected = true
        chooseThirdObstacleButton.isSelected = false
    }
    @IBAction func chooseThirdObstacleButton(_ sender: UIButton) {
        obstacleImageView.image = UIImage(named: Constants.obstaclesArray[2])
        self.settings?.obstacle = Constants.obstaclesArray[2]
        self.carImageView.contentMode = .scaleAspectFit
        chooseFirstObstacleButton.isSelected = false
        chooseSecondObstacleButton.isSelected = false
        chooseThirdObstacleButton.isSelected = true
    }
    @IBAction func closeScreenSettingsButton(_ sender: UIButton) {

        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func tapDetected(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
    }
    
    // MARK: - Private functions
    
    private func registerForKeyboardNotifications() {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    
    @objc private func keyboardWillShow(_ notification: NSNotification) {

            let userInfo = notification.userInfo!
            let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue
            let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue

            if notification.name == UIResponder.keyboardWillHideNotification {
                bottomConstraint.constant = 0
            } else {
                bottomConstraint.constant = keyboardScreenEndFrame.height
                scrollView.contentOffset = CGPoint(x: 0, y: scrollView.contentSize.height)
            }

            view.needsUpdateConstraints()

            UIView.animate(withDuration: animationDuration) {
                self.view.layoutIfNeeded()
            }
        }
    
    // MARK: - Flow functions

    func setSettings() {
        carImageView.image = UIImage(named: settings?.car ?? "grayCarImage")
        carImageView.contentMode = .scaleAspectFit
        obstacleImageView.image = UIImage(named: settings?.obstacle ?? "stoneImage")
        obstacleImageView.contentMode = .scaleAspectFit
        nameTextField.text = settings?.username
        
        self.imageView.image = UIImage(named: "flagImage")
        self.imageView.contentMode = .scaleAspectFill
//        self.imageView.alpha = 0.5
        
        switch settings?.car {
        case "grayCarImage":
            chooseFirstCarButton.isSelected = true
        case "redCarImage":
            chooseSecondCarButton.isSelected = true
        case "blueCarImage":
            chooseThirdCarButton.isSelected = true
        default:
            return
        }
        switch settings?.obstacle {
        case "stoneImage":
            chooseFirstObstacleButton.isSelected = true
        case "roadWork":
            chooseSecondObstacleButton.isSelected = true
        case "barrierImage":
            chooseThirdObstacleButton.isSelected = true
        default:
            return
        }
    }
    func setTitles() {
        chooseFirstCarButton.setTitle("1".localized(), for: .normal)
        chooseFirstCarButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        chooseSecondCarButton.setTitle("2".localized(), for: .normal)
        chooseSecondCarButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        chooseThirdCarButton.setTitle("3".localized(), for: .normal)
        chooseThirdCarButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        chooseFirstObstacleButton.setTitle("Stone".localized(), for: .normal)
        chooseFirstObstacleButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        chooseSecondObstacleButton.setTitle("Sign".localized(), for: .normal)
        chooseSecondObstacleButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        chooseThirdObstacleButton.setTitle("Barrier".localized(), for: .normal)
        chooseThirdObstacleButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        buttonSavedSettings.setTitle("SAVE".localized(), for: .normal)
        buttonSavedSettings.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        fillInLabel.text = "FILL IN YOUR NAME".localized()
        fillInLabel.font = UIFont(name: Constants.font, size: 20)
    }
}

    // MARK: - Extensions

extension SettingsViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameTextField.resignFirstResponder()
        return true
    }
}
