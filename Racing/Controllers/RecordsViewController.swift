import UIKit

class RecordsViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var tableNameLabel: UILabel!
    @IBOutlet weak var backToMenuButton: UIButton!
    
    // MARK: - Public Properties
    
    var results = [RaceResults]()
    var newResult: RaceResults?
    var array = [RaceResults]()
    var newArray = [RaceResults]()
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        removeResultsFromArray()
        
        setSettings()
        
    }
    override func viewDidLayoutSubviews() {
        self.imageView.image = UIImage(named: "flagImage")
        self.imageView.contentMode = .scaleAspectFill
    }

    // MARK: - IBActions

    @IBAction func closeScreenRecordsButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Flow functions

    func removeResultsFromArray() {
        self.results = RaceResultsManager.shared.getArray() ?? []
        if results.count > 20 {
            var newArray = results.sorted(by: {$0.getScore() > $1.getScore()})
            newArray.removeSubrange(20..<results.count)
            print(newArray.count)
            RaceResultsManager.shared.setArray(newArray)

        }
    }
    func setSettings() {
        backToMenuButton.setTitle("MENU".localized(), for: .normal)
        self.tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        self.array = RaceResultsManager.shared.getArray() ?? []
        self.newArray = array.sorted(by: {$0.getScore() > $1.getScore()})
        self.newResult = RaceResultsManager.shared.getResults()
        print(newResult)
        tableNameLabel.font = UIFont(name: Constants.font, size: 20)
        tableNameLabel.text = "GAME RESULTS".localized()
        backToMenuButton.titleLabel?.font = UIFont(name: Constants.font, size: 15)
    }
}

// MARK: - Extensions

extension RecordsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.newArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        let result = newArray[indexPath.row]
        if indexPath.row == 0 {
            cell.customImageView.image = UIImage(named: "gold")
            cell.customImageView.contentMode = .scaleAspectFill
        } else if indexPath.row == 1 {
            cell.customImageView.image = UIImage(named: "silver")
            cell.customImageView.contentMode = .scaleAspectFill
        } else if indexPath.row == 2 {
            cell.customImageView.image = UIImage(named: "bronze")
            cell.customImageView.contentMode = .scaleAspectFill
            
        }
        cell.setScoreCell(cell: cell, result: result, array: newArray)
        cell.layer.backgroundColor = UIColor.clear.cgColor
        cell.countLabel.text = String(indexPath.row + 1)
        cell.countLabel.font = UIFont(name: Constants.font, size: 15)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Constants.heightForResultsRow
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
