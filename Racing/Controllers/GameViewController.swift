import UIKit
import CoreMotion

class GameViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var leftBushesView: UIView!
    @IBOutlet weak var rightBushesView: UIView!
    @IBOutlet weak var leftBushImageView: UIImageView!
    @IBOutlet weak var rightBushImageView: UIImageView!
    @IBOutlet weak var roadView: UIView!
    @IBOutlet weak var carXConstraint: NSLayoutConstraint!
    @IBOutlet weak var upRightBushConstraint: NSLayoutConstraint!
    @IBOutlet weak var bigView: UIView!
    @IBOutlet weak var carImageView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var carView: UIView!
    
    // MARK: - Public Properties
    
    var obstaclesArray: [UIImageView] = []
    var score = Int()
    var newSettings: Settings?
    var results = RaceResults()
    var currentX = 0.0
    var motionManager = CMMotionManager()
    var moveTimer = Timer()
    var jumpTimer = Timer()
    var leftBushTimer = Timer()
    var rightBushTimer = Timer()
    var stripTimer = Timer()
    var stoneTimer = Timer()
    var accidentCheckTimer = Timer()
    var checkScoreTimer = Timer()
    override var prefersStatusBarHidden: Bool {
        return true
    }
    let car = UIImageView()
    let stoneImageView = UIImageView()
    
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        self.moveTimer.invalidate()
        if event?.subtype == UIEvent.EventSubtype.motionShake {
            accidentCheckTimer.invalidate()
            UIView.animate(withDuration: 0.5, animations: {
                self.car.transform = CGAffineTransform.init(scaleX: 1.7, y: 1.7)
            }) { (_) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.car.transform = CGAffineTransform.identity

                }) { (_) in
                    self.checkAccident()
                    self.movingWithAccelerometer()
                }
            }
        }
    }
    
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        motionManager.accelerometerUpdateInterval = 0.02
        motionManager.startAccelerometerUpdates()
        motionManager.gyroUpdateInterval = 0.02
        motionManager.startGyroUpdates()
        
      let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm"
        let date = dateFormatter.string(from: Date.init())
        print(date)
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(jumpedCar(_:)))
        recognizer.numberOfTapsRequired = 2
        self.view.addGestureRecognizer(recognizer)
        
        self.newSettings = SettingsManager.shared.getSettings()
        self.results = RaceResultsManager.shared.getResults()
        self.results.username = self.newSettings?.username
        
        let when = DispatchTime.now() + 1
        
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.movingWithAccelerometer()
        }
        self.leftBushAnimation()
        self.rightBushAnimation()
        self.loadCar()
        self.creatingStrip()
        self.creatingStone()
        self.checkAccident()
        self.startStrips(y: 0)
        self.startStrips(y: self.roadView.frame.size.height / 3)
        self.startStrips(y: self.roadView.frame.size.height / 3 * 2)
        self.results.date = date
        
    }
    override func viewDidLayoutSubviews() {
        setColors()

        if self.obstaclesArray.count == 0 {
            self.scoreLabel.text = "SCORE:".localized() + " \(self.obstaclesArray.count)"
        } else {
            self.scoreLabel.text = "SCORE:".localized() + " \(self.obstaclesArray.count - 1)"
        }

        
    }
    
    // MARK: - Flow functions

    func leftBushAnimation() {
        leftBushTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { (_) in
            
            let leftBush = UIImageView()
            leftBush.frame = CGRect(x: self.leftBushesView.frame.origin.x, y: Constants.bushesStart, width: self.leftBushesView.frame.size.width, height: self.leftBushesView.frame.size.width)
            leftBush.image = UIImage(named: Constants.namesArray.randomElement() ?? "bushImage")
            leftBush.contentMode = .scaleToFill
            self.view.addSubview(leftBush)
            
            UIView.animate(withDuration: 5) {
                leftBush.frame.origin.y = Constants.bushesFinish
            }
        })
        leftBushTimer.fire()
    }
    
    func rightBushAnimation() {
        
        rightBushTimer = Timer.scheduledTimer(withTimeInterval: 1.3, repeats: true, block: { (_) in
            let rightBush = UIImageView()
            rightBush.frame = CGRect(x: self.bigView.frame.size.width - self.rightBushesView.frame.size.width, y: Constants.bushesStart, width: self.rightBushesView.frame.size.width, height: self.rightBushesView.frame.size.width)
            rightBush.image = UIImage(named: Constants.namesArray.randomElement() ?? "treeImage")
            rightBush.contentMode = .scaleToFill
            self.view.addSubview(rightBush)
            UIView.animate(withDuration: 5) {
                rightBush.frame.origin.y = Constants.bushesFinish
                
            }
        })
        rightBushTimer.fire()
    }
    
    func creatingStrip() {
        stripTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { (_) in
            let roadStrip = UIView()
            roadStrip.frame = CGRect(x: self.roadView.frame.size.width / 2, y: Constants.stripsStart, width: Constants.widthOfStrip, height: Constants.heightOfStrip)
            roadStrip.backgroundColor = .white
            self.roadView.addSubview(roadStrip)
            UIView.animate(withDuration: 5) {
                roadStrip.frame.origin.y = Constants.bushesFinish
            }
        })
    }
    
    func loadCar() {
        self.car.frame = self.carImageView.frame
        self.car.frame.origin.x = self.view.frame.size.width / 2
        self.car.image = UIImage(named: newSettings?.car ?? "grayCarImage")
        self.view.addSubview(self.car)
    }
    
    func creatingStone() {
        stoneTimer = Timer.scheduledTimer(withTimeInterval: 3.5, repeats: true
            , block: { (_) in
                
                let x = CGFloat.random(in: self.leftBushesView.frame.size.width...self.rightBushesView.frame.origin.x - Constants.widthOfStones)
                self.stoneImageView.frame = CGRect(x: x, y: Constants.stonesStart, width: Constants.widthOfStones, height: Constants.heightOfStones)
                self.stoneImageView.image = UIImage(named: self.newSettings?.obstacle ?? "stoneImage")
                self.carView.addSubview(self.stoneImageView)
                self.obstaclesArray.append(self.stoneImageView)
                
                self.checkScoreTimer.fire()

                UIView.animate(withDuration: 5) {
                    self.stoneImageView.frame.origin.y = Constants.stonesFinish
                    
                }
        })
    }
    
    func checkAccident() {
        accidentCheckTimer = Timer.scheduledTimer(withTimeInterval: 0.05, repeats: true, block: { (_) in
            if self.car.frame.intersects(self.stoneImageView.layer.presentation()?.frame ?? CGRect(x: 0, y: 0, width: 0, height: 0)) {
                self.accidentCheckTimer.invalidate()
                self.moveTimer.invalidate()
                self.stoneTimer.invalidate()
                self.leftBushTimer.invalidate()
                self.rightBushTimer.invalidate()
                self.view.layer.removeAllAnimations()
                let blackView = UIView()
                blackView.frame = self.view.frame
                blackView.backgroundColor = .black
                blackView.alpha = 0
                UIView.animate(withDuration: 0.5) {
                       blackView.alpha = 1
                self.view.addSubview(blackView)
                   }
                self.createAlert()

            }
        })
        accidentCheckTimer.fire()
    }
    
    func finishGame() {
        guard let gameOverController = self.storyboard?.instantiateViewController(withIdentifier: "GameOverViewController") as? GameOverViewController else {
            return
        }
        self.score = self.obstaclesArray.count
        print(score)
        if score == 0 {
            self.results.score = score
        } else {
            self.results.score = score - 1
        }
        
        RaceResultsManager.shared.setResults(results)
        
        gameOverController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(gameOverController, animated: true)
    }
    
    func movingWithAccelerometer() {
        self.moveTimer = Timer.scheduledTimer(withTimeInterval: 0.02, repeats: true, block: { (timer) in
            let accelerometer = self.motionManager.accelerometerData?.acceleration.x

            if self.motionManager.accelerometerData?.acceleration.x ?? 0.0 < -0.05 {
                if self.car.frame.origin.x > self.leftBushesView.frame.size.width - Constants.plusSize {
                    UIView.animate(withDuration: 0.1) {
                        self.car.frame.origin.x -= Constants.moving
                    }
                } else {
                    self.accidentCheckTimer.invalidate()
                    self.moveTimer.invalidate()
                    self.stoneTimer.invalidate()
                    self.leftBushTimer.invalidate()
                    self.rightBushTimer.invalidate()
                    self.view.layer.removeAllAnimations()
                    var blackView = UIView()
                    blackView.frame = self.view.frame
                    blackView.backgroundColor = .black
                    blackView.alpha = 0
                    UIView.animate(withDuration: 0.5) {
                           blackView.alpha = 1
                    self.view.addSubview(blackView)
                       }
                    self.createAlert()

                }
            } else if self.motionManager.accelerometerData?.acceleration.x ?? 0.0 > 0.05 {
                if self.car.frame.origin.x + self.car.frame.size.width < self.rightBushesView.frame.origin.x + Constants.plusSize {
                    UIView.animate(withDuration: 0.1) {
                        self.car.frame.origin.x += Constants.moving
                    }
                } else {
                    self.accidentCheckTimer.invalidate()
                    self.moveTimer.invalidate()
                    self.stoneTimer.invalidate()
                    self.leftBushTimer.invalidate()
                    self.rightBushTimer.invalidate()
                    self.view.layer.removeAllAnimations()
                    let blackView = UIView()
                    blackView.frame = self.view.frame
                    blackView.backgroundColor = .black
                    blackView.alpha = 0
                    UIView.animate(withDuration: 0.5) {
                           blackView.alpha = 1
                    self.view.addSubview(blackView)
                       }
                    self.createAlert()
                }

            }
            self.currentX = accelerometer ?? 0.0
        })
        moveTimer.fire()
    }

    @objc func jumpedCar(_ sender:UITapGestureRecognizer) {
        self.moveTimer.invalidate()
            accidentCheckTimer.invalidate()
            UIView.animate(withDuration: 0.5, animations: {
                self.car.transform = CGAffineTransform.init(scaleX: 1.7, y: 1.7)
            }) { (_) in
                UIView.animate(withDuration: 0.5, animations: {
                    self.car.transform = CGAffineTransform.identity

                }) { (_) in
                    self.checkAccident()
                    self.movingWithAccelerometer()
                }
            }
    }
    
    func startStrips(y: CGFloat) {
        let roadStrip = UIView()
        roadStrip.frame = CGRect(x: self.roadView.frame.size.width / 2, y: y, width: Constants.widthOfStrip, height: Constants.heightOfStrip)
        roadStrip.backgroundColor = .white
        self.roadView.addSubview(roadStrip)
        UIView.animate(withDuration: 5) {
            roadStrip.frame.origin.y = Constants.stripsFinish
        }
    }
    
    func createAlert() {
        let alertCtrl = UIAlertController(title: "THANK YOU", message: "You have to enter your name", preferredStyle: .alert)
        alertCtrl.addTextField { (text) in
            text.textColor = .black
            text.placeholder = "Enter your name"
            text.text = self.newSettings?.username
        }
        let saveAction = UIAlertAction(title: "SAVE", style: .default) { (_) in
            let textField = alertCtrl.textFields?[0]
            self.results.username = textField?.text
            self.newSettings?.username = textField?.text
            guard let settings = self.newSettings else {return}
            SettingsManager.shared.setSettings(settings)
            
            self.finishGame()
        }
        alertCtrl.addAction(saveAction)
        self.present(alertCtrl, animated: true)
    }
    func setColors() {
      self.view.backgroundColor = .lightGray
        self.leftBushesView.backgroundColor = #colorLiteral(red: 0.09546028823, green: 0.5711315274, blue: 0.09241846949, alpha: 1)
        self.leftBushesView.contentMode = .scaleAspectFill
        self.leftBushesView.layer.borderWidth = 1
        self.leftBushesView.layer.borderColor = UIColor.gray.cgColor
        self.rightBushesView.backgroundColor = #colorLiteral(red: 0.09269923717, green: 0.5407527685, blue: 0.09478154033, alpha: 1)
        self.rightBushesView.layer.borderWidth = 1
        self.rightBushesView.layer.borderColor = UIColor.gray.cgColor
        self.roadView.backgroundColor = .lightGray
        scoreLabel.font = UIFont(name: Constants.font, size: 15)
    }

}





