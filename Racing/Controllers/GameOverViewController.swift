import UIKit

class GameOverViewController: UIViewController {

    // MARK: - IBOutlets
    
    @IBOutlet weak var buttonReturnToMenu: UIButton!
    @IBOutlet weak var gameOverImageView: UIImageView!
    @IBOutlet weak var resultLabel: UILabel!
    
    // MARK: - Public Properties
    
    var newResult: RaceResults?
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Lifecycle functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        setSettings()
        
    }
    
    // MARK: - IBActions

    @IBAction func buttonReturnToMenu(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    // MARK: - Flow functions
    
    func setSettings() {
        gameOverImageView.image = UIImage(named: "gameOver")
               gameOverImageView.contentMode = .scaleToFill
               buttonReturnToMenu.setTitle("RETURN TO THE MENU".localized(), for: .normal)
              buttonReturnToMenu.titleLabel?.font = UIFont(name: Constants.font, size: 20)
               self.newResult = RaceResultsManager.shared.getResults()
        resultLabel.text = "\(String(newResult?.username ?? "noname")), " + "your score is".localized() + " \(Int(newResult?.score ?? 0))"
        resultLabel.font = UIFont(name: Constants.font, size: 15)
    }

}
