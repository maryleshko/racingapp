import UIKit
import FirebaseCrashlytics

class ViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var openRecordsButton: UIButton!
    @IBOutlet weak var openSettingsButton: UIButton!
    @IBOutlet weak var menuImageView: UIImageView!
    
    // MARK: - Public Properties
    
    var settings = Settings()
    var results = [RaceResults]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    // MARK: - Lifecycle functions

    override func viewDidLoad() {
        super.viewDidLoad()
        self.settings = SettingsManager.shared.getSettings()
        
        setTitles()
        
    }
    override func viewDidLayoutSubviews() {
        
        self.addParallaxToView(view: menuImageView, magnitude: 30)
        menuImageView.image = UIImage(named: "menuImage")
        menuImageView.contentMode = .scaleAspectFill
        
    }
   
    // MARK: - IBActions
    
    @IBAction func startGameButton(_ sender: UIButton) {
        guard let gameController = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as? GameViewController else {
            return
        }
        gameController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(gameController, animated: true)
    }
    
    @IBAction func openRecordsButton(_ sender: UIButton) {
        guard let recordsController = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as? RecordsViewController else {
            return
        }
        recordsController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(recordsController, animated: true)
    }
    
    @IBAction func openSettingsButton(_ sender: UIButton) {
        guard let settingsController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {
            return
        }
        settingsController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(settingsController, animated: true)
    }
    
    // MARK: - Flow functions

    func addParallaxToView(view: UIView, magnitude: Float) {
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -magnitude
        horizontal.maximumRelativeValue = magnitude

        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -magnitude
        vertical.maximumRelativeValue = magnitude

        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        view.addMotionEffect(group)
    }
    func setTitles() {
        startGameButton.roundCorners()
        startGameButton.dropShadow(color: .black, offSet: CGSize(width: 5, height: 5))
        startGameButton.addGradient()
        startGameButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        
        
        openRecordsButton.roundCorners()
        openRecordsButton.dropShadow(color: .black, offSet: CGSize(width: 5, height: 5))
        openRecordsButton.addGradient()
        openRecordsButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        
        openSettingsButton.roundCorners()
        openSettingsButton.dropShadow(color: .black, offSet: CGSize(width: 5, height: 5))
        openSettingsButton.addGradient()
        openSettingsButton.titleLabel?.font = UIFont(name: Constants.font, size: 20)
        
        openRecordsButton.setTitle("RECORDS".localized(), for: .normal)
        openSettingsButton.setTitle("SETTINGS".localized(), for: .normal)
        
        let firstText = "START ".localized()
        let secondText = "GAME".localized()
        
        let firstAttributes: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor : UIColor.black]
        let secondAttributes : [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor : UIColor.white]
        let firstAttributedString = NSMutableAttributedString(string: firstText, attributes: firstAttributes)
        let secondAttributedString = NSAttributedString(string: secondText, attributes: secondAttributes)
        firstAttributedString.append(secondAttributedString)
        startGameButton.setAttributedTitle(firstAttributedString, for: .normal)
    }
}

