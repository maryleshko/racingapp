import Foundation
import UIKit

class Constants {
    static let carsArray: [String] = ["grayCarImage", "redCarImage", "blueCarImage"]
    static let obstaclesArray: [String] = ["stoneImage", "roadWork", "barrierImage"]
    static let namesArray: [String] = ["bushImage", "treeImage", "bushRedImage"]
    static let bushesStart: CGFloat = -200
    static let bushesFinish: CGFloat = 1200
    static let stripsStart: CGFloat = -50
    static let widthOfStrip: CGFloat = 5
    static let heightOfStrip: CGFloat = 50
    static let stripsFinish: CGFloat = 1300
    static let stonesFinish: CGFloat = 1200
    static let heightOfStones: CGFloat = 50
    static let widthOfStones: CGFloat = 50
    static let stonesStart: CGFloat = -50
    static let moving: CGFloat = 2
    static let heightForResultsRow: CGFloat = 100
    static let font = "AkzidenzGroteskPro-MedExtIt"
    static let plusSize: CGFloat = 15


}

