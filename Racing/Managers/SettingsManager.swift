import Foundation
import UIKit

class SettingsManager {
    static let shared = SettingsManager()
    
    private var defaultSettings = Settings(car: "grayCarImage", obstacle: "stoneImage", username: "noname")
    let key = "settings"
    
    func getSettings() -> Settings {
        if let settings = UserDefaults.standard.value(Settings.self, forKey: key) {
            return settings
        }
        return defaultSettings
    }
    func setSettings(_ settings: Settings) {
        UserDefaults.standard.set(encodable: settings, forKey: key)
    }
}
extension UserDefaults {

    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }

    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}
