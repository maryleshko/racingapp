import Foundation
import UIKit

class RaceResultsManager {
    static let shared = RaceResultsManager()
    
//    var resultsArray = [RaceResults]()
    
    private var defaultResults = RaceResults(username: "noname", score: 0, date: "")
    let key = "results"
    
    func getResults() -> RaceResults {
        if let resultsArray = self.getArray() {
            return resultsArray.last ?? defaultResults
        }
        return defaultResults

    }
    func getArray() -> [RaceResults]? {

        if let resultsArray = UserDefaults.standard.value([RaceResults].self, forKey: key) {
//            let newArray = resultsArray.sorted(by: {$0.getScore() > $1.getScore()})
            return resultsArray
        }
        return []
    }
    func setResults(_ results: RaceResults) {
        var resultsArray = self.getArray()
        resultsArray?.append(results)
        self.setArray(resultsArray ?? [])
        print(resultsArray?.count ?? 0)
    }
    func setArray(_ resultsArray: [RaceResults]) {
        
        
        UserDefaults.standard.set(encodable: resultsArray, forKey: key)
//        print([resultsArray].count)
    }
    
//    func addResult(_ results: RaceResults) {
//        if var resultsArray = self.getArray() {
//            resultsArray.append(results)
//            self.setResults(results)
//        } else {
//            return
//        }
//    }
}

//if let results = UserDefaults.standard.value([RaceResults].self, forKey: key) {
//        if let resultsArray = UserDefaults.standard.value([RaceResults].self, forKey: key)
//        if var array = resultsArray {
////            return [results.last ?? defaultResults]
//            array.sorted(by: {$0.getScore() > $1.getScore()})
//            return array
//        }
//        return []
