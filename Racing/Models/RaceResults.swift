import UIKit

class RaceResults: Codable {

    
    var username: String?
    var score: Int?
    var date: String?
    
    
    init() {}
    
    init(username: String?, score: Int?, date: String?) {
        self.username = username
        self.score = score
        self.date = date
    }
    
    public enum CodingKeys: String, CodingKey {
        case username, score, date
    }
   
    required public init(from decoder: Decoder) throws {
           let container = try decoder.container(keyedBy: CodingKeys.self)
        self.username = try container.decodeIfPresent(String.self, forKey: .username)
        self.score = try container.decodeIfPresent(Int.self, forKey: .score)
        self.date = try container.decodeIfPresent(String.self, forKey: .date)
       }

       public func encode(to encoder: Encoder) throws {
           var container = encoder.container(keyedBy: CodingKeys.self)
           try container.encode(self.username, forKey: .username)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.date, forKey: .date)
       }
    func getScore() -> Int {
        return self.score ?? 0
    }
    func getName() -> String {
        return self.username ?? ""
    }
    func getDate() -> String {
        return self.date ?? ""
    }

}
