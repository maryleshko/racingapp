import UIKit

class Settings: Codable {

    
    var car: String?
    var obstacle: String?
    var username: String?
    
    init() {}
    
    init(car: String?, obstacle: String?, username: String?) {
        self.car = car
        self.obstacle = obstacle
        self.username = username
    }
    
    public enum CodingKeys: String, CodingKey {
        case car, obstacle, username
    }
   
    required public init(from decoder: Decoder) throws {
           let container = try decoder.container(keyedBy: CodingKeys.self)
        self.car = try container.decodeIfPresent(String.self, forKey: .car)
        self.obstacle = try container.decodeIfPresent(String.self, forKey: .obstacle)
        self.username = try container.decodeIfPresent(String.self, forKey: .username)
       }

       public func encode(to encoder: Encoder) throws {
           var container = encoder.container(keyedBy: CodingKeys.self)
           try container.encode(self.car, forKey: .car)
           try container.encode(self.obstacle, forKey: .obstacle)
           try container.encode(self.username, forKey: .username)
       }

}

//extension UserDefaults {
//
//    func set<T: Encodable>(encodable: T, forKey key: String) {
//        if let data = try? JSONEncoder().encode(encodable) {
//            set(data, forKey: key)
//        }
//    }
//
//    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
//        if let data = object(forKey: key) as? Data,
//            let value = try? JSONDecoder().decode(type, from: data) {
//            return value
//        }
//        return nil
//    }
//}
