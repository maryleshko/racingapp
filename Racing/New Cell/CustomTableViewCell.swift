import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    
    var counter = 0
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    override func prepareForReuse() {
        customImageView.image = nil
    }
    
    func setScoreCell(cell: CustomTableViewCell, result: RaceResults, array: [RaceResults]) {
        cell.scoreLabel.font = UIFont(name: Constants.font, size: 17)
        cell.scoreLabel.text = "Name: ".localized() + "\(result.getName()), " + "score: ".localized() + "\(result.getScore())"
        cell.dateLabel.font = UIFont(name: Constants.font, size: 17)
        cell.dateLabel.text = "Date: ".localized() + " \(result.getDate())"
        
    }
}
